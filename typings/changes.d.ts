import { Document } from "mongoose";
import { ObjectID } from "mongodb";

export interface IOwnerDocument extends Document, IOwner {
}

export interface IOwner
{
    ownerID: String,
    ownerPSW: String
}

export interface IPetDocument extends Document, IPet {
}

export interface IPet {
    ownerID: String,
    name: String,
    born: Date,
    dead: Date,
    note: String,
    age: Number
}

type ValidateFunction = (value: any, propertyName: string) => void;
export interface Interval {
    start: Date;
    end?: Date;
}
