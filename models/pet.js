var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PetSchema = new Schema({
    ownerID: String,
    name: String,
    born: Date,
    dead: Date,
    note: String,
    age: Number
});

module.exports = mongoose.model('Pet', PetSchema);