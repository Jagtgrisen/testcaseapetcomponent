var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OwnerSchema = new Schema({
    ownerID: String,
    ownerPSW: String
});

module.exports = mongoose.model('Owner', OwnerSchema);