import { expect } from "chai";
import _ = require("lodash");
import * as moment from "moment";
import { IPet, ValidateFunction } from "../typings/changes";
import { largerOrEqualZero } from "./NumberValidators";
import { notNull } from "./ObjectValidators";
import { notEmpty, notNullOrEmpty, notUndefined } from "./StringValidators";

import { isNumber } from "util";
import Validator from "./Validator";

export const PetRules: IRuleDictionary = {
    ownerID: [notNull, notEmpty, notUndefined],
    name: [notNull, notEmpty, notUndefined],
    born: [notNull],
    dead: [],
    note: [notNull, notEmpty, notUndefined],
    age: [ isNumber, largerOrEqualZero ],
};

interface IRuleDictionary {
    [key: string]: ValidateFunction[];
}

export default class PetValidator {
    private pet: IPet;
    public constructor(pet: IPet) {
        this.pet = pet;
    }

    public ValidateBirthDeathAge = () => {
        notNull(this.pet, "Pet");
        const validator = new Validator<IPet>(this.pet);
        validator.Validate("born", "Pet", ...PetRules.born);
        validator.Validate("dead", "Pet", ...PetRules.dead);
        validator.Validate("age", "Pet", ...PetRules.age);
        const momentBorn = moment(this.pet.born);

        expect(momentBorn.valueOf(), "born should be in the past")
            .to.be.lessThan(new Date().valueOf());
        let ageCompareMoment = moment();
        if (!_.isNull(this.pet.dead)) {
            ageCompareMoment = moment(this.pet.dead);
            expect(momentBorn.valueOf(), "born should be before dead")
                .to.be.lessThan(ageCompareMoment.valueOf());
        }
        const calculatedAge = ageCompareMoment.subtract(momentBorn.year(), "year");
        expect(this.pet.age, `age should be ${calculatedAge.year()}`).to.be.equal(calculatedAge.year());
    }
}
