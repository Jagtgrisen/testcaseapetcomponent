import { expect } from "chai";
import _ = require("lodash");
import * as moment from "moment";
import { Interval, ValidateFunction } from "../typings/changes";
import { notNull } from "./ObjectValidators";

export const notBefore = (dateInterval: Interval, propertyName: string): ValidateFunction =>
    expect(dateInterval[propertyName], `${propertyName} should not be before`).to.not.be.undefined;
export const notAfter = (dateInterval: Interval, propertyName: string): ValidateFunction =>
    expect(dateInterval[propertyName], `${propertyName} should not be before`).to.not.be.undefined;
export const isValidISO8601 = (dateProperty: Date, propertyName: string): ValidateFunction =>
    expect(isValidFormat(dateProperty, moment.ISO_8601),
        `${propertyName} should be in correct format`).to.be.true;
const isValidFormat = (dateProperty, format: moment.MomentBuiltinFormat) =>
    moment(dateProperty, format).isValid();
