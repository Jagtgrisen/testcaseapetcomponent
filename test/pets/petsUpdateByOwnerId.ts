describe("Test PUT by ownerID on /pets/", () => {
    it("ownerID should exist");
    it("born date should be past");
    it("dead date should not be future");
    it("born date should not be after dead date");
    it("age should be death date - start date");
});
