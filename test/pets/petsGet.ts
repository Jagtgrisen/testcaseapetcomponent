import { expect, request, use } from "chai";
import * as moment from "moment";
import { ObjectID } from "mongodb";
import * as mongoose from "mongoose";
import { Config } from "../../config";
import { IOwner, IOwnerDocument, IPet, IPetDocument } from "../../typings/changes";
import { notNull } from "../ObjectValidators";
import { notNullOrEmpty } from "../StringValidators";
import Validator from "../Validator";
import PetValidator, { PetRules } from "./../PetRules";
import { notEmpty, notUndefined } from "./../StringValidators";

describe("Test GET on /pets/", () => {
    const testOwner: IOwnerDocument = {} as any;
    const testPet: IPetDocument = {} as any;
    const environment = Config.GetConfig("development");
    const baseRequestUrl = Config.BaseRequestUrl(environment);
    const agent = request(baseRequestUrl);
    beforeEach("Removing all owners and pets, then seeding database with test data", (done) => {
        const connection = mongoose.createConnection(environment.dbPath).on("open", (result) => {
            const Pet: mongoose.Model<IPetDocument> = connection.model("Pet");
            const Owner: mongoose.Model<IOwnerDocument> = connection.model("Owner");
            Owner.remove({}, (errOwn) => {
                if (errOwn) {
                    done(errOwn);
                    return;
                }
                Pet.remove({}, (errPet) => {
                    if (errPet) {
                        done(errPet);
                        return;
                    }
                    const owner: IOwner = {
                        ownerPSW: "1234",
                    } as any;
                    Owner.create(owner).then((own) => {
                        const pet: IPet = {
                            ownerID: own._id, // ownerID should not be a property on owner document
                            age: 5,
                            dead: new Date(),
                            born: moment().subtract("year", 5).toDate(),
                            name: "Test pet 1",
                            note: "Test pet note 1",
                        };
                        Pet.create(pet, (createdPet) => {
                            connection.close().then(() => done());
                        });
                    });
                });
            });
        });
    });

    it("should be able to get 200 on get request", (done) => {
        const promise = agent.get("/pets/");
        promise.then((res) => {
            expect(res, "Response should be not null").to.not.be.equal(null);
            expect(res.status, "status should not be above 299").to.not.be.above(299);
            expect(res.status, "status should not be below 200").to.not.be.below(200);
            done();
        },
        (err) => {
            done(err);
        });
    });
    it("should return valid pet", (done) => {
        const promise = agent.get("/pets/");
        promise.then((res) => {
            const pets: IPet[] = res.body;
            pets.forEach((pet) => {
                const validator = new Validator<IPet>(pet);
                validator.Validate("name", "Pet", ...PetRules.name);
                validator.Validate("ownerID", "Pet", ...PetRules.ownerID);
                validator.Validate("note", "Pet", ...PetRules.note);
                const petValidator = new PetValidator(pet);
                petValidator.ValidateBirthDeathAge();
            });
            done();
        },
        (err) => {
            done(err);
        });
    });
    it("should be able to get a list of pets which is not empty");
});
