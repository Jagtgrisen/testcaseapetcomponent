import { expect, request, use } from "chai";
import * as moment from "moment";
import * as mongoose from "mongoose";
import { IOwner, IOwnerDocument, IPetDocument } from "../../typings/changes";
import { Config } from "./../../config";
const chaiHttp = require("chai-http");
use(chaiHttp);
const environment = Config.GetConfig(process.env.NODE_ENV);
const baseRequestUrl = Config.BaseRequestUrl(environment);

const Pet: mongoose.Model<IPetDocument> = require("../../models/pet");
const Owner: mongoose.Model<IOwnerDocument> = require("../../models/owner");

describe("Test Post on /pets/", () => {
    const agent = request(baseRequestUrl);
    let testOwner: IOwnerDocument = {} as any;
    let testPet: IPetDocument = {} as any;

    beforeEach((done) => {
        mongoose.connect(environment.dbPath);
        Owner.remove({}).then(() => {
            return Owner.create({
                ownerPSW: "1234",
            }).then((owner) => {
                expect(owner).to.not.equal(null);
                testOwner = Object.assign({}, owner);
                const birthDate = moment("2014-01-01");
                const deathDate = moment("2018-01-01");
                return Pet.create({
                    ownerID: owner._id,
                    name: "Felix",
                    born: birthDate.toDate(),
                    dead: deathDate.toDate(),
                    note: "This is a cat",
                    age: deathDate.diff(birthDate, "year"),
                }, (errPet, pet) => {
                    if (errPet) {
                        done(errPet);
                    }
                    expect(pet).to.not.equal(null);
                    expect(pet.ownerID).to.be.equal(owner.ownerID);
                    testPet = Object.assign({}, pet);
                    done();
                });
            }).catch((errOwner) => done(errOwner));
        });
    });
    it("ownerID should exist", (done) => {
        done();
        // const pet: IPet {
        //     ownerID: testOwner.
        // }
        // agent.post("/pets/").send();
    });
    it("born date should be past");
    it("dead date should not be future");
    it("born date should not be after dead date");
    it("age should be death date - start date");
});
