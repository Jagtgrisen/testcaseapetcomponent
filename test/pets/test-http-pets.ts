import { expect, request, use } from "chai";
import * as _ from "lodash";
import * as moment from "moment";
import * as mongoose from "mongoose";
import { IOwner, IOwnerDocument, IPet, IPetDocument } from "../../typings/changes";
import { Config } from "./../../config";

const Pet: mongoose.Model<IPetDocument> = require("../../models/pet");
const Owner: mongoose.Model<IOwnerDocument> = require("../../models/owner");

const chaiHttp = require("chai-http");
use(chaiHttp);

describe("Pets Testing", () => {
    const environment = Config.GetConfig(process.env.NODE_ENV);
    const baseRequestUrl = Config.BaseRequestUrl(environment);
    const agent = request(baseRequestUrl);

    const owner: IOwner = null;
    // before((done) => {
    //     mongoose.connect(environment.dbPath);
    //     it("Create Owner to DB", (ownDone) => {
    //         const ownerPromise = Owner.create({
    //             ownerID: mongoose.Types.ObjectId(),
    //             ownerPSW: "1234",
    //             }, (err, res) => {
    //                 if (err) {
    //                     ownDone(err);
    //                 } else {
    //                     owner = res;
    //                     ownDone();
    //                 }
    //             });
    //     });

    //     it("Create Pet to DB", (petDone) => {
    //         const birthDate = moment("01-01-2014");
    //         const deathDate = moment("01-01-2018");
    //         const petPromise = Pet.create({
    //             //id: mongoose.Types.ObjectId(),
    //             name: "Teeest1",
    //             born: birthDate.toDate(),
    //             dead: deathDate.toDate(),
    //             ownerID: owner.ownerID,
    //             note: "hejsa0",
    //             age: deathDate.diff(birthDate, "year"),
    //         }, (err, res) => {
    //             if (err) {
    //                 petDone(err);
    //             } else {
    //                 petDone();
    //             }
    //         });
    //     });
    // });

    // describe ("Pet GET", () => {
    //     it("should be able to get 200 on get request", (done) => {
    //         const promise = request(baseRequestUrl).get("/pets/");
    //         promise.then((res) => {
    //             expect(res, "response should not be null").to.not.be.equal(null);
    //             expect(res.status, "status should not be above 299").to.not.be.above(299);
    //             expect(res.status, "status should not be below 200").to.not.be.below(200);
    //             done();
    //         },
    //         (err) => {
    //             done(err);
    //         });
    //     });
    //     it("should be able to get a list of pets which is not empty");
    // });

    // const createPostPet = (startDate: moment.Moment, endDate: moment.Moment) => {
    //     const promise = agent.post("/pets/");
    //     const pet: IPet = {
    //         name: "Teeest1",
    //         born: startDate.toDate(),
    //         dead: endDate.toDate(),
    //         ownerID: owner.ownerID,
    //         note: "hejsa0",
    //         age: endDate.diff(endDate, "year"),
    //     };
    //     promise.send(pet);
    //     return promise;
    // }

    // describe("Pets POST", () => {
    //     it("should be able to add a new pet", (done) => {
    //         const currentDate = moment("01-01-2017 00:00:00");
    //         const deadDate = currentDate.add(5, "day");
    //         const promise = createPostPet(currentDate, deadDate);
    //         expect((promise as any).url ).to.be.equal("http://localhost:3000/pets/");
    //         promise.then((res) => {
    //             expect(res, "response should not be null").to.not.be.equal(null);
    //             expect(res.status, "status should not be above 299").to.not.be.above(299);
    //             expect(res.status, "status should not be below 200").to.not.be.below(200);
    //             done();
    //         },
    //         (err) => {
    //             done(err);
    //         });
    //     });
    //     it("should ");
    // });

    // describe("Pets PUT", () => {
    //     it("should be able to change existing pet");
    //     it("should not be able to change a pet which doesn't exist")
    // });

    // describe("Pets DELETE", () => {
    //     it("should be able to delete new pet");
    //     it("should not be possible to delete pet which doesn't exist");
    // });

});
