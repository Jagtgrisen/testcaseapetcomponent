import { expect, request, use } from "chai";
import * as mongoose from "mongoose";
import { IOwner, IOwnerDocument, IPetDocument } from "../../typings/changes";
import { Config } from "./../../config";
use(require("chai-http"));

const Pet: mongoose.Model<IPetDocument> = require("../../models/pet");
const Owner: mongoose.Model<IOwnerDocument> = require("../../models/owner");

const environment = Config.GetConfig(process.env.NODE_ENV);
const baseRequestUrl = Config.BaseRequestUrl(environment);
describe("Status and content", () => {
    let owner: IOwner = null;
    before((done) => {
        mongoose.connect(environment.dbPath);
        Owner.create({
            ownerID: mongoose.Types.ObjectId(),
            ownerPSW: "1234",
            }, (err, res) => {
                if (err) {
                    done(err);
                } else {
                    owner = res;
                    done();
                }
            });
    });
    describe ("Reach server", () => {
        it("should be able to get 200 on get request", (done) => {

            const promise = request(baseRequestUrl).get("/pets/");
            expect((promise as any).url ).to.be.equal("http://localhost:3000/pets/");
            promise.then((res) => {
                expect(res, "response should not be null").to.not.be.equal(null);
                expect(res.status, "status should not be above 299").to.not.be.above(299);
                expect(res.status, "status should not be below 200").to.not.be.below(200);
                done();
            },
            (err) => {
                done(err);
            });
        });
    });

    describe("Owner Login", () => {
        it("should be able to login with owner", (done) => {
            const promise = request(baseRequestUrl).post("/login/");
            promise.send(owner);
            promise.then((res) => {
                expect(res, "response should not be null").to.not.be.equal(null);
                expect(res.status, "status should not be above 299").to.not.be.above(299);
                expect(res.status, "status should not be below 200").to.not.be.below(200);
                done();
            },
            (err) => {
                done(err);
            });
        });
    });

});
