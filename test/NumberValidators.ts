import { expect } from "chai";
import _ = require("lodash");
import { ValidateFunction } from "../typings/changes";
import { notNull } from "./ObjectValidators";

export const largerThanZero = (numbr: number, propertyName: string): ValidateFunction =>
    expect(numbr, `${propertyName} should be larger than Zero`).to.be.above(0);
export const largerOrEqualZero = (numbr: number, propertyName: string): ValidateFunction =>
    expect(numbr, `${propertyName} should be larger than or equal Zero`).to.be.least(0);
export const isNumber = (numbr: number, propertyName: string): ValidateFunction =>
    expect(numbr, `${propertyName} should be a number`).to.not.be.NaN;
export const notNegative = (numbr: number, propertyName: string): ValidateFunction =>
    expect(numbr, `${propertyName} should not be below Zero`).to.not.below(0);
