import { expect } from "chai";
import _ = require("lodash");
import { ValidateFunction } from "../typings/changes";

export const notNull = (object: any, propertyName: string): ValidateFunction =>
    expect(object, `${propertyName} should not be null`).to.be.not.null;
