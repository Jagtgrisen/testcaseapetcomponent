import { expect } from "chai";
import { ValidateFunction } from "../typings/changes";
import { notNull } from "./ObjectValidators";

export default class Validator<T> {
    private object: T;
    public constructor(object: T) {
        this.object = object;
    }

    public Validate(propertyName: string, objectName: string, ...funcs: ValidateFunction[]): void {
        notNull(propertyName, objectName);
        funcs.map((func) => func(this.object[propertyName], propertyName));
    }
}
