import { expect } from "chai";
import _ = require("lodash");
import { ValidateFunction } from "../typings/changes";
import { notNull } from "./ObjectValidators";

export const notNullOrEmpty = (stringProperty: string, propertyName: string): ValidateFunction =>
    expect(stringProperty, `${propertyName} should not be null or empty`)
        .to.not.be.null
        .and.to.not.be.equal("null")
        .and.to.not.be.empty;

export const notEmpty = (stringProperty: string, propertyName: string): ValidateFunction =>
    expect(stringProperty, `${propertyName} should not be empty`).to.not.be.empty;
export const notUndefined = (stringProperty: string, propertyName: string): ValidateFunction =>
    expect(stringProperty, `${propertyName} should not be undefined`).to.not.be.undefined;
