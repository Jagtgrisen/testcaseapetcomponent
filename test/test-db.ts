import { expect, use } from "chai";
import { createConnection } from "mongoose";
import { Config } from "./../config";
const chai = use(require("chai-http"));

describe("Database preconditions", () => {
    describe ("Mongodb running", () => {
        it("running", (done) => {
            const environment = Config.GetConfig(process.env.NODE_ENV);
            expect(environment.dbPath).to.be.equal(`mongodb://localhost:27017/PetDB`);
            const connection = createConnection(environment.dbPath).on("open", (result) => {
                expect((connection.db.databaseName)).to.equal("PetDB");
                done();
                connection.close();
            });
        });
    });
});
