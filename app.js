
// BASE FOR THE SERVER 
var express = require('express');

var app = express();

var mongoose = require('mongoose');

var bodyParser = require('body-parser');

var urlencode = bodyParser.urlencoded({ extended: true });
app.use(express.static('public'));

app.use(bodyParser.json());


// MAKE DB CONNECTION

mongoose.connect('mongodb://localhost:27017/PetDB');

// MODELS 

var Owner = require('./models/owner');

var Pet = require('./models/pet');


// ROUTING

var router = express.Router();

// TO PROCESS THE NEXT REQUEST !!

router.use(function (req, res, next) {
    console.log("(debug) some action happening...");
    next();
});

app.use('/', router); 

/*  HELPER   */
function dateDiffInYears(dateold, datenew) {
  var ynew = datenew.getFullYear();
  var mnew = datenew.getMonth();
  var dnew = datenew.getDate();
  var yold = dateold.getFullYear();
  var mold = dateold.getMonth();
  var dold = dateold.getDate();
  var diff = ynew - yold;
  if (mold > mnew) diff--;
  else {
      if (mold == mnew) {
          if (dold > dnew) diff--;
      }
  }
  return diff;
}

router.route('/pets')
    .get(function (req, res) {
        Pet.find({ }, { _id:0,__v: 0 },function (err, pets) {
            if (err)
                res.status(500).send(err);
            res.status(200).json(pets);
        });
    })
    
    .post(function (req, res) {
        var pet = new Pet(req.body); 
        
        if (pet.age !== dateDiffInYears(pet.born,pet.dead)){
          res.status(422).send("the age is not correct");
        }
        pet.save(function (err) {
            if (err)
                res.status(500).send(err);
            res.status(201).json(pet);
        });
    });
/*   LOGIN TO ENCRYPTION NO REAL SECURITY !!!!! */
router.route('/login')
    
    .post(function (req, res) {
        var actualOwner = new Owner(req.body); 
        console.log("owner ID:" + actualOwner.ownerID);
        console.log("owner password:" + actualOwner.ownerPSW);
        Owner.findOne({ownerID:actualOwner.ownerID},function (err, owner) {
            if (!owner){
                res.status(403).send("access denied");
            }
            if (err){
                res.status(403).send("access denied");
            }  
            if (actualOwner){
               if ( actualOwner.ownerPSW === owner.ownerPSW){
                    res.status(200).send("access granted");
               }
            }
        });
        
    });

router.route('/pets/:owner_id')
    .get(function (req, res) {
        Pet.findOne({ ownerID: req.params.owner_id }, {_id:0,__v:0}, function (err, pet) {
            if (err)
                res.status(500).send(err);
            res.status(200).json({ pet });
        });
    })
    
    .put(function (req, res) {
        Pet.findOneAndReplace({ ownerID: req.params.owner_id },req.body, {_id:0,__v:0}, function (err, pet) {
            if (err){
                res.status(500).send(err);
            }
            res.status(203).json({ pet });
        });
      })
     .delete(function (req, res) {
        Pet.findOneAndDelete({ ownerID: req.params.owner_id }, {_id:0,__v:0}, function (err, pet) {
            if (err){
               res.status(500).send(err);
            }
            else if(!pet){
               res.status(404).send(err);
            }        
            res.status(203).json({ pet });
      });
    });

    



// SERVER START

app.listen(3000, () => {
    console.log('We are now listening on port 3000 (serverside)');
});


