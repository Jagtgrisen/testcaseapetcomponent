import { IConfigEnvironment } from "./config";
export class Config {

    public static BaseRequestUrl = (config: IConfigEnvironment) => `${config.serverPath}:${config.serverPort}`;

    public static GetConfig = (environment: string): IConfigEnvironment => {
        if (!environment) {
            return Config.environments.development;
        } else {
            return Config.environments[environment];
        }
    }
    private static environments: IEnvironments = {
        development: {
            dbPath: `mongodb://localhost:27017/PetDB`,
            serverPath: "http://localhost",
            serverPort: 3000,
        },
        production: {
            dbPath: `mongodb://localhost:27017/PetDB`,
            serverPath: "http://localhost",
            serverPort: 3000,
        },
    };
}

export interface IConfigEnvironment {
    dbPath: string;
    serverPath: string;
    serverPort: number;
}

export interface IEnvironments {
    development: IConfigEnvironment;
    production: IConfigEnvironment;
}
